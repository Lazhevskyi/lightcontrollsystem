-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: light_controll
-- ------------------------------------------------------
-- Server version	5.6.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_token`
--

DROP TABLE IF EXISTS `access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `access_token_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_token`
--

LOCK TABLES `access_token` WRITE;
/*!40000 ALTER TABLE `access_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regionId` int(11) NOT NULL,
  `districtId` int(11) NOT NULL,
  `cityId` int(11) NOT NULL,
  `streetId` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `address_id_uindex` (`id`),
  KEY `address_city_id_fk` (`cityId`),
  KEY `address_region_id_fk` (`regionId`),
  KEY `address_district_id_fk` (`districtId`),
  KEY `address_street_id_fk` (`streetId`),
  CONSTRAINT `address_city_id_fk` FOREIGN KEY (`cityId`) REFERENCES `city` (`id`),
  CONSTRAINT `address_district_id_fk` FOREIGN KEY (`districtId`) REFERENCES `district` (`id`),
  CONSTRAINT `address_region_id_fk` FOREIGN KEY (`regionId`) REFERENCES `region` (`id`),
  CONSTRAINT `address_street_id_fk` FOREIGN KEY (`streetId`) REFERENCES `street` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(1) DEFAULT NULL,
  `lastName` char(1) DEFAULT NULL,
  `login` char(1) DEFAULT NULL,
  `email` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_id_uindex` (`id`),
  UNIQUE KEY `admins_email_uindex` (`email`),
  UNIQUE KEY `admins_login_uindex` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(1) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lan` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `city_id_uindex` (`id`),
  UNIQUE KEY `city_title_uindex` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `control_command`
--

DROP TABLE IF EXISTS `control_command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control_command` (
  `id` int(11) NOT NULL,
  `code` char(1) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `control_command_code_uindex` (`code`),
  UNIQUE KEY `control_command_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control_command`
--

LOCK TABLES `control_command` WRITE;
/*!40000 ALTER TABLE `control_command` DISABLE KEYS */;
/*!40000 ALTER TABLE `control_command` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `district`
--

DROP TABLE IF EXISTS `district`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(1) NOT NULL,
  `description` text,
  `lat` float DEFAULT NULL,
  `lan` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `district_id_uindex` (`id`),
  UNIQUE KEY `district_title_uindex` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `district`
--

LOCK TABLES `district` WRITE;
/*!40000 ALTER TABLE `district` DISABLE KEYS */;
/*!40000 ALTER TABLE `district` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `global_network`
--

DROP TABLE IF EXISTS `global_network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `global_network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` char(1) NOT NULL,
  `addressId` int(11) NOT NULL,
  `title` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `network_id_uindex` (`id`),
  UNIQUE KEY `network_ip_uindex` (`ip`),
  UNIQUE KEY `network_addressId_uindex` (`addressId`),
  CONSTRAINT `network_address_id_fk` FOREIGN KEY (`addressId`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `global_network`
--

LOCK TABLES `global_network` WRITE;
/*!40000 ALTER TABLE `global_network` DISABLE KEYS */;
/*!40000 ALTER TABLE `global_network` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local_network`
--

DROP TABLE IF EXISTS `local_network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `local_network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `globalNetworkId` int(11) NOT NULL,
  `networkId` int(11) NOT NULL,
  `nodeId` int(11) NOT NULL,
  `addressId` int(11) NOT NULL,
  `title` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `local_network_id_uindex` (`id`),
  KEY `local_network_global_network_id_fk` (`globalNetworkId`),
  KEY `local_network_address_id_fk` (`addressId`),
  CONSTRAINT `local_network_address_id_fk` FOREIGN KEY (`addressId`) REFERENCES `address` (`id`),
  CONSTRAINT `local_network_global_network_id_fk` FOREIGN KEY (`globalNetworkId`) REFERENCES `global_network` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local_network`
--

LOCK TABLES `local_network` WRITE;
/*!40000 ALTER TABLE `local_network` DISABLE KEYS */;
/*!40000 ALTER TABLE `local_network` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `node_characteristic`
--

DROP TABLE IF EXISTS `node_characteristic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_characteristic` (
  `temperature` float DEFAULT NULL,
  `voltage` float DEFAULT NULL,
  `brightness` int(11) DEFAULT NULL,
  `description` text,
  `nodeId` int(11) NOT NULL,
  PRIMARY KEY (`nodeId`),
  CONSTRAINT `node_characteristic_local_network_id_fk` FOREIGN KEY (`nodeId`) REFERENCES `local_network` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `node_characteristic`
--

LOCK TABLES `node_characteristic` WRITE;
/*!40000 ALTER TABLE `node_characteristic` DISABLE KEYS */;
/*!40000 ALTER TABLE `node_characteristic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `node_state`
--

DROP TABLE IF EXISTS `node_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nodeId` int(11) NOT NULL,
  `stateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `node_state_id_uindex` (`id`),
  UNIQUE KEY `node_state_nodeId_uindex` (`nodeId`),
  KEY `node_state_state_id_fk` (`stateId`),
  CONSTRAINT `node_state_local_network_id_fk` FOREIGN KEY (`nodeId`) REFERENCES `local_network` (`id`),
  CONSTRAINT `node_state_state_id_fk` FOREIGN KEY (`stateId`) REFERENCES `state` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `node_state`
--

LOCK TABLES `node_state` WRITE;
/*!40000 ALTER TABLE `node_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `node_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(1) NOT NULL,
  `description` text,
  `lat` float DEFAULT NULL,
  `lan` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `region_id_uindex` (`id`),
  UNIQUE KEY `region_title_uindex` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(1) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `state_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `street`
--

DROP TABLE IF EXISTS `street`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `street` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(1) DEFAULT NULL,
  `descriptin` varchar(500) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lan` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `street_id_uindex` (`id`),
  UNIQUE KEY `street_title_uindex` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `street`
--

LOCK TABLES `street` WRITE;
/*!40000 ALTER TABLE `street` DISABLE KEYS */;
/*!40000 ALTER TABLE `street` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-07  7:37:02
